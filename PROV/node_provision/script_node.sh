#! /bin/bash

# Déclaration variables
## Gère le fichier hosts pour pas ajouter de doublons lors du reload via vagrant
FILE_HOSTS_ORIGIN="/etc/hosts.ori"
## Home de vagrant
HOME_VAGRANT="/home/vagrant"
## User Ansible
USER_ANSIBLE="user-ansible"
## Password Ansible
PASSWORD_ANSIBLE="ansible"
## Group user Ansible
GROUP_ANSIBLE="user-ansible"
## Home du user ansible
HOME_USER_ANSIBLE="/home/$USER_ANSIBLE"
## User Docker
USER_DOCKER="user-docker"
## Password Ansible
PASSWORD_DOCKER="docker"

# Mettre à jour CentOS
sudo yum update -y

# Création de l'utilisateur Ansible
sudo adduser "$USER_ANSIBLE"
echo "$USER_ANSIBLE:$PASSWORD_ANSIBLE" | sudo chpasswd
## Rendre sudoers le user-ansible (sous CentOS le groupe "wheel" peut lancer toute les commandes)
sudo usermod -a -G wheel $USER_ANSIBLE

# Ajout des hosts name dans /etc/hosts
if [ -f "$FILE_HOSTS_ORIGIN" ]; then
    cp $FILE_HOSTS_ORIGIN /etc/hosts
    cat $HOME_VAGRANT/PROV/all_provision/hosts | sudo tee -a /etc/hosts
else
    cp /etc/hosts $FILE_HOSTS_ORIGIN
    cat $HOME_VAGRANT/PROV/all_provision/hosts | sudo tee -a /etc/hosts
fi
echo "/etc/hosts updated"

# !!!NE PAS FAIRE EN ENTREPRISE!!! Désactiver le SELinux de manière permanente
#echo "SELINUX=permissive" | sudo tee -a /etc/selinux/config

# !!!NE PAS FAIRE EN ENTREPRISE!!! Désactiver le firewalld de manière permanente
#sudo systemctl disable firewalld.service

# Copie de la clé public ssh pour permettre la connexion depuis le node manager dans le home de user-ansible
if [ ! -d "$HOME_USER_ANSIBLE/.ssh" ]; then
    mkdir "$HOME_USER_ANSIBLE/.ssh"
    echo "$HOME_USER_ANSIBLE/.ssh created"
fi 
cp $HOME_VAGRANT/PROV/ssh/node/* "$HOME_USER_ANSIBLE/.ssh"
chown -R "$USER_ANSIBLE:$GROUP_ANSIBLE" "$HOME_USER_ANSIBLE/.ssh"
chmod 700 "$HOME_USER_ANSIBLE/.ssh"
chmod 600 "$HOME_USER_ANSIBLE/.ssh/authorized_keys"
echo "ssh files copied"
