#! /bin/bash

# Déclaration variables
## Gère le fichier hosts pour pas ajouter de doublons lors du reload via vagrant
FILE_HOSTS_ORIGIN="/etc/hosts.ori"
## Gère le fichier source pour pas ajouter de doublons lors du reload via vagrant
FILE_SOURCE_ORIGIN="/etc/apt/sources.list.ori"
## Home de vagrant
HOME_VAGRANT="/home/vagrant"
## User Ansible
USER_ANSIBLE="user-ansible"
## Password Ansible
PASSWORD_ANSIBLE="ansible"
## Group user Ansible
GROUP_ANSIBLE="user-ansible"
## Home du user ansible
HOME_USER_ANSIBLE="/home/$USER_ANSIBLE"

# Mettre à jour Debian
sudo apt update -y
sudo apt upgrade -y

# Activer le repository PPA pour avoir la dernière version Ansible puis installe Ansible
sudo apt install dirmngr -y
## Ajoute en fin de fichier le repository
if [ ! -f "$FILE_SOURCE_ORIGIN" ]; then
    cp /etc/apt/sources.list "$FILE_SOURCE_ORIGIN" 
    echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" | sudo tee -a /etc/apt/sources.list
fi
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
sudo apt update -y
sudo apt install ansible -y
echo "Ansible Installed"



# Création de l'utilisateur "user-ansible" avec mot de passe "ansible"
sudo adduser "$USER_ANSIBLE" --gecos ",,," --disabled-password
echo "$USER_ANSIBLE:$PASSWORD_ANSIBLE" | sudo chpasswd

# Ajout des hosts name dans /etc/hosts
if [ -f "$FILE_HOSTS_ORIGIN" ]; then
    cp "$FILE_HOSTS_ORIGIN" /etc/hosts
    cat "$HOME_VAGRANT/PROV/all_provision/hosts" | sudo tee -a /etc/hosts
else
    cp /etc/hosts "$FILE_HOSTS_ORIGIN"
    cat "$HOME_VAGRANT/PROV/all_provision/hosts" | sudo tee -a /etc/hosts
fi
echo "/etc/hosts updated"

# Copie de la clé privé ssh pour permettre la connexion depuis le node manager dans le home de user-ansible
if [ ! -d "$HOME_USER_ANSIBLE/.ssh" ]; then
    mkdir "$HOME_USER_ANSIBLE/.ssh"
    echo "$HOME_USER_ANSIBLE/.ssh created"
fi 
cp $HOME_VAGRANT/PROV/ssh/node_manager/* "$HOME_USER_ANSIBLE/.ssh"
chown -R "$USER_ANSIBLE:$GROUP_ANSIBLE" "$HOME_USER_ANSIBLE/.ssh"
chmod 700 "$HOME_USER_ANSIBLE/.ssh"
chmod 600 "$HOME_USER_ANSIBLE/.ssh/id_rsa"
echo "ssh files copied"

# Copie des fichiers nécessaires à l'installation de Docker
cp $HOME_VAGRANT/PROV/node_manager_provision/ansible_init/* "$HOME_USER_ANSIBLE/"
chown -R "$USER_ANSIBLE:$GROUP_ANSIBLE" "$HOME_USER_ANSIBLE/"
echo "ansible files copied"