### Author : Gilbert CAMBUSIER

# Mise en place d'une infrastructure sous Debian, CentOS avec installation d'Ansible et Docker avec Vagrant
- 1 Node Manager : Debian 9 avec Ansible
- 2 Nodes : CentOS 7 avec Docker

## Prérequis :
- Avoir [git](https://gitforwindows.org/) sur sa machine pour cloner le projet
- Installer [Oracle VirtualBox](https://www.virtualbox.org/wiki/Downloads)
- Installer [Vagrant](https://www.vagrantup.com/downloads)

## Données :
- utilisateur Vagrant (login/mdp) : vagrant/vagrant (pour accéder aux nodes en ssh)
- utilisateur Ansible (login/mdp) : user-ansible/ansible (à utiliser pour lancer des playbook par exemple)

## Ce qu'il faut faire :
1. Cloner ce projet
2. Lancer un terminal en vous plaçant dans le répertoire tout juste cloné et faire : <pre><code>vagrant up</code></pre>
3. Attendre que l'étape 2 se termine
4. Connectez-vous au node manager en ssh, en se plaçant dans le dossier cloné et faire : <pre><code>vagrant ssh ansible</code></pre>
5. Changez d'utilisateur : <pre><code>su user-ansible</code></pre>
6. Générer les fingerprints en vous connectant une première fois sur chaque node en faisant : <pre><code>ssh node1</code></pre> puis <pre><code>exit</code></pre> puis <pre><code>ssh node2</code></pre> (bien accepter les fingerprints)
7. Se placer dans le home de user-ansible (node manager) et faire (pour installer docker) : <pre><code>ansible-playbook -i inventory.ini --user user-ansible --become --ask-become-pass install_docker.yml</code></pre>
8.  Attendre que l'étape 6 se termine
9.  Pour tester docker, vous pouvez vous connecter à un des nodes et changer d'utilisateur en faisant : <pre><code>ssh node1</code></pre> puis faire un <pre><code>docker run hello-world</code></pre>

Voilà tout est prêt! Vous pouvez maintenant créer et gérer des conteneurs sur les nodes avec Docker et gérer vos machines avec Ansible.
   