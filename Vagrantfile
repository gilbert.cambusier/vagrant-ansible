# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANT_VERSION = "2"

Vagrant.configure(VAGRANT_VERSION) do |config|

  # 1ere VM : Machine qui contiendra le gestionnaire Ansible
  config.vm.define "ansible" do |ansible|
    ansible.vm.hostname = "ansible"
    # se connecte sur https://vagrantcloud.com/search
    # puis télécharge la VM
    ansible.vm.box = "generic/debian9"
    # autorise la VM à accéder à Internet et on défini l'IP du réseau dans lequel la machine sera (à changer selon son besoin)
    ansible.vm.network "public_network", ip: "192.168.122.10", bridge: "eth0"

    # Copie des fichiers nécessaires sur la VM
    ansible.vm.provision "file", source: "./PROV", destination: "~/PROV"

    # Nous utilisons le plugin "shell" pour récupérer un fichier directement depuis l'hote et l'exécuter sur la VM
    ansible.vm.provision "shell", path: "./PROV/node_manager_provision/script_node_manager.sh"

    # configuration pour le hyperviseur Hyper-V
    ansible.vm.provider "hyperv" do |hv|
      hv.memory = "1024" # je mets 1Go de Ram
      hv.cpus = "1" # je donne 1 CPU
    end

    # configuration du matériel si je suis sur virtualbox
    ansible.vm.provider "virtualbox" do |vb|
      vb.memory = "1024" # je mets 1Go de Ram
      vb.cpus = "1" # je donne 1 CPU
    end
  end

  # 2eme VM : VM qui sera provisionnée par le gestionnaire Ansible
  config.vm.define "node1" do |node1|
    node1.vm.hostname = "node1"
    node1.vm.box = "generic/centos7"
    node1.vm.network "public_network", ip: "192.168.122.11", bridge: "eth0"
    node1.vm.provision "file", source: "./PROV", destination: "~/PROV"
    node1.vm.provision "shell", path: "./PROV/node_provision/script_node.sh"

    node1.vm.provider "hyperv" do |hv|
      hv.memory = "2048"
      hv.cpus = "2"
    end

    node1.vm.provider "virtualbox" do |vb|
      vb.memory = "2048"
      vb.cpus = "2"
    end
  end

  # 3eme VM : VM qui sera provisionnée par le gestionnaire Ansible
  config.vm.define "node2" do |node2|
    node2.vm.hostname = "node2"
    node2.vm.box = "generic/centos7"
    node2.vm.network "public_network", ip: "192.168.122.12", bridge: "eth0"
    node2.vm.provision "file", source: "./PROV", destination: "~/PROV"
    node2.vm.provision "shell", path: "./PROV/node_provision/script_node.sh"


    node2.vm.provider "hyperv" do |hv|
      hv.memory = "2048"
      hv.cpus = "2"
    end

    node2.vm.provider "virtualbox" do |vb|
      vb.memory = "2048"
      vb.cpus = "2"
    end
  end
end